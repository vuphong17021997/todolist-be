
var express = require('express');
var app = express();
var bodyParser = require('body-parser');
var mongoose = require('mongoose')
task = require ('./api/model/model')

mongoose.connect('mongodb://localhost/task',{
    useNewUrlParser:true,
    useCreateIndex: true,
    useUnifiedTopology:true,
})

app.use(bodyParser.urlencoded({extended:true}));
app.use(bodyParser.json());

var route = require('./api/route/routes')
route(app);

app.listen(3001,()=>{
    console.log("connected to port 3002")
})