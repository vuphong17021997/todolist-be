
task = require('mongoose').model('task')
function responseDTO(status, res, mes) {
    response = {
        status: status,
        response: res,
        message: mes
    }
    return response
}
exports.get_all_tasks = function (req, res) {
    task.find({}, function (err, data) {
        let listTask = []
        if (err) {
            res.send(responseDTO(false, null, err));
        } else {

            data.forEach(element => {
                let task = {
                    id: element._id,
                    task: element.task,
                    status: element.status[0],
                    order: element.order
                }
                listTask = [...listTask, task]
            });
        }
        let newTask = listTask.sort((a, b) => a.order - b.order)
        res.json(responseDTO(true, newTask, null))
    })
}
exports.create_a_task = async function (req, res) {
    var new_task = new task(req.body);
    let tasks = await task.find({}).exec()
    new_task.order = tasks.length + 1
    new_task.save(function (err, task) {
        if (err) {
            res.send(responseDTO(false, null, err));
        } else {
            let taskDTO = {
                id: task._id,
                task: task.task,
                order: tasks.length + 1,
                status: task.status[0]
            }
            res.status(200).json(responseDTO(true, taskDTO, null))
        }
    })
};
exports.update_a_task = async function (req, res) {

    let oldTask = await task.findById(req.params.taskId, function (err, data) {
        if (err) throw err
        return data
    })
   

    let updateTask = {
        order: req.body.order
    }
 

    task.findOneAndUpdate({ _id: req.params.taskId }, updateTask, {
        new: true
    },
    
        function (err, updatedTask) {
            if (err) {
                res.send(responseDTO(false, null, err));
            } else {
                let taskDTO = {
                    id: updatedTask._id,
                    task: updatedTask.task,
                    status: updatedTask.status[0]
                }
                res.status(200).json(responseDTO(true, taskDTO, null))
            }
        })
};

exports.delete_a_task = function (req, res) {
    task.findByIdAndDelete(req.params.taskId, function (err) {
        if (err) throw err
        res.status(200).json(responseDTO(true, null, "deleted thanh cong hoho"))
    })
}









exports.read_a_task = function (req, res) {
    task.findById(req.params.taskId, function (err, data) {
        if (err) throw err
        res.json(data)
    })
};